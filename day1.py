def solve_day1a():
    freq = 0
    with open("day1_input.txt") as data_file:
        for line in data_file:
            freq += int(line)

    print(freq)

def solve_day1b():
    freq = 0
    freqs = set([0])

    while True:
        with open("day1_input.txt") as data_file:
            for line in data_file:
                freq += int(line)

                if freq in freqs:
                    print(freq)
                    return

                freqs.add(freq)
    

if __name__ == '__main__':
    solve_day1a()
    solve_day1b()