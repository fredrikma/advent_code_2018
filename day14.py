
def solve_day14a():
    recipes = [3,7]
    elf_1 = 0
    elf_2 = 1

    num_recipes = 540391+10
    while len(recipes) < num_recipes:
        combined = str(recipes[elf_1] + recipes[elf_2])

        # add new recipes
        for r in combined:
            recipes.append(int(r))

        # step
        elf_1 += 1 + recipes[elf_1]
        elf_2 += 1 + recipes[elf_2]

        elf_1 = elf_1 % len(recipes)
        elf_2 = elf_2 % len(recipes)

    print(''.join([ str(c) for c in recipes[-10:]]))

def solve_day14b():
    recipes = '37'
    elf_1 = 0
    elf_2 = 1
    
    round = 0
    while True:
        round += 1
        if round % 100000 == 0:
            p = recipes.find('540391')
            if p != -1:
                print(p)
                break

        combined = str(int(recipes[elf_1]) + int(recipes[elf_2]))

        # add new recipes
        recipes += combined

        # step
        elf_1 += 1 + int(recipes[elf_1])
        elf_2 += 1 + int(recipes[elf_2])

        elf_1 = elf_1 % len(recipes)
        elf_2 = elf_2 % len(recipes)

if __name__ == '__main__':
    solve_day14a()    
    solve_day14b()