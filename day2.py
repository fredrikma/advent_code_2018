from collections import Counter

def solve_day2a():
    two_count = 0
    three_count = 0

    with open("day2_input.txt") as data_file:

        for id in data_file:
            counts = Counter(id).values()
            if 3 in counts:
                three_count +=1
            if 2 in counts:
                two_count += 1

    print(two_count * three_count)


def solve_day2b():

    ids = list(open("day2_input.txt"))
    ids = list(map(lambda x: x.rstrip('\n'), ids))

    for i in range(len(ids)-1):
        c1 = ids[i]
        for j in range(i+1, len(ids)):
            c2 = ids[j]
            s = ''.join(map(lambda x: x[0], filter(lambda x: x[0] == x[1], zip(c1, c2))))
            if len(s) + 1 == len(c1):
                print(s)
                return

if __name__ == '__main__':
    solve_day2a()
    solve_day2b()