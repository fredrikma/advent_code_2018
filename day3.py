import re


def decode(s):
    id = int(re.search('#\d+', s).group(0)[1:])
    left = int(re.search('@ \d+', s).group(0)[2:])
    bottom = int(re.search(',\d+', s).group(0)[1:])
    sx = int(re.search(': \d+', s).group(0)[2:])
    sy = int(re.search('x\d+', s).group(0)[1:])

    return (id, left, bottom, left+sx-1, bottom+sy-1)

def fill(d, s):
    (_, min_x1, min_y1, max_x1, max_y1) = decode(s)

    for x in range(min_x1, max_x1+1):
        for y in range(min_y1, max_y1+1):
            key = '{0}x{1}'.format(x,y)
            if not key in d:
                d[key] = 0
            else:
                d[key] = 1

def is_alone(d, s):
    (_, min_x1, min_y1, max_x1, max_y1) = decode(s)

    for x in range(min_x1, max_x1+1):
        for y in range(min_y1, max_y1+1):
            key = '{0}x{1}'.format(x,y)
            if d[key] != 0:
                return False

    return True

def solve_day3ab():

    claims = list(open("day3_input.txt"))
    claims = list(map(lambda x: x.rstrip('\n'), claims))

    fabric = dict()
    for c in claims:
        fill(fabric, c)
        
    print(sum(fabric.values()))

    for c in claims:
        if is_alone(fabric, c):
            print(c)
            return

if __name__ == '__main__':
    solve_day3ab()
