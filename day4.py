import re

def solve_day4a():

    logs = list(open("day4_input.txt"))
    logs = sorted(list(map(lambda x: x.rstrip('\n'), logs)))

    sleepcycle = {}

    guard_id = None
    curr_day = None
    for l in logs:

        if '#' in l:
            guard_id = int(re.search('#\d+', l).group(0)[1:])
            #print('Guard {0}'.format(guard_id))

            min_start_sleep = None
            min_wakeup = None

        elif 'falls asleep' in l:
            min_start_sleep = int(re.search(':\d+', l).group(0)[1:])
            curr_day = re.search('-\d+-\d+ ', l).group(0)[1:]

            #print('{0} Start sleep {1}'.format(curr_day, min_start_sleep))

            if not guard_id in sleepcycle:
                sleepcycle[guard_id] = {}

            if not curr_day in sleepcycle[guard_id]:
                sleepcycle[guard_id][curr_day] = ['.' for _ in range (60)]

        elif 'wakes up' in l:
            min_wakeup = int(re.search(':\d+', l).group(0)[1:])
            #print('{0} Stop sleep {1}'.format(curr_day, min_wakeup))

        if min_start_sleep and min_wakeup:

            for m in range(min_start_sleep, min_wakeup):
                sleepcycle[guard_id][curr_day][m] = '#'

            min_start_sleep = None
            min_wakeup = None


    longest_guard_id = None
    longest_duration = 0

    for guard_id, dates in sleepcycle.items():
        sleep_duration = 0
        for date, minutes in dates.items():
            d = minutes.count('#')
            sleep_duration += d
            if sleep_duration > longest_duration:
                longest_duration = sleep_duration
                longest_guard_id = guard_id

    dates = sleepcycle[longest_guard_id].items()
    dates = [y for x, y in dates]
    
    longest = 0
    longest_m = 0
    for m in range(60):
        cnt = 0
        for day in dates:
            if day[m] == '#':
                cnt += 1
        if cnt > longest:
            longest = cnt
            longest_m = m
            

    print(longest_m * longest_guard_id)

def solve_day4b():

    logs = list(open("day4_input.txt"))
    logs = sorted(list(map(lambda x: x.rstrip('\n'), logs)))

    sleepcycle = {}

    guard_id = None
    curr_day = None
    for l in logs:

        if '#' in l:
            guard_id = int(re.search('#\d+', l).group(0)[1:])
            #print('Guard {0}'.format(guard_id))

            min_start_sleep = None
            min_wakeup = None

        elif 'falls asleep' in l:
            min_start_sleep = int(re.search(':\d+', l).group(0)[1:])
            curr_day = re.search('-\d+-\d+ ', l).group(0)[1:]

            #print('{0} Start sleep {1}'.format(curr_day, min_start_sleep))

            if not guard_id in sleepcycle:
                sleepcycle[guard_id] = {}

            if not curr_day in sleepcycle[guard_id]:
                sleepcycle[guard_id][curr_day] = ['.' for _ in range (60)]

        elif 'wakes up' in l:
            min_wakeup = int(re.search(':\d+', l).group(0)[1:])
            #print('{0} Stop sleep {1}'.format(curr_day, min_wakeup))

        if min_start_sleep and min_wakeup:

            for m in range(min_start_sleep, min_wakeup):
                sleepcycle[guard_id][curr_day][m] = '#'

            min_start_sleep = None
            min_wakeup = None


    longest_guard_id = None

    long_freq = 0
    long_freq_m = 0

    for guard_id, dates in sleepcycle.items():
        dates = [y for x, y in dates.items()]

        freq = 0
        freq_m = 0
        for m in range(60):
            cnt = 0
            for day in dates:
                if day[m] == '#':
                    cnt += 1
            if cnt > freq:
                freq = cnt
                freq_m = m
        
        if freq > long_freq:
            long_freq = freq
            long_freq_m = freq_m
            longest_guard_id = guard_id

    print(longest_guard_id * long_freq_m)


if __name__ == '__main__':
    solve_day4a()
    solve_day4b()
