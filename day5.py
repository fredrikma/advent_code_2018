
def solve_day5a():
    with open("day5_input.txt") as data_file:
        polymer = data_file.read().rstrip('\n')
        print(len(reduce(polymer)))

def solve_day5b():
    with open("day5_input.txt") as data_file:
        polymer = data_file.read().rstrip('\n')
        
        chars = [x for x in range(ord('A'), ord('Z'))]
        shortest = len(polymer)

        for c in chars:
            cl = c + ord('a') - ord('A')
            polymer_reduced = polymer.replace(chr(c), '').replace(chr(cl), '')

            shortest = min(len(reduce(polymer_reduced)), shortest)

        print(shortest)

def reduce(s):
    s = list(s)
    i = 0
    while i < len(s) - 1:
        diff = abs(ord(s[i]) - ord(s[i+1]))
        if diff == ord('a') - ord('A'):
            del s[i:(i+2)]
            i = max(0, i-1)
        else:
            i += 1

    return ''.join(s)

if __name__ == '__main__':
    solve_day5a()
    solve_day5b()