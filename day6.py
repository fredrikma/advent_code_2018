from itertools import product
from collections import Counter

def closest(x, y, coords):
    ds = []

    for i, c in enumerate(coords):
        dc = abs(c[0] - x) + abs(c[1] - y)
        ds.append([dc, i])

    ds = sorted(ds)

    if ds[0][0] == ds[1][0]:
        return 0

    return ds[0][1]+1

def solve_day6a():

    coords = list(open("day6_input.txt"))
    coords = list(map(lambda x: x.rstrip('\n'), coords))
    coords = list(map(lambda x: [int(y) for y in x.split(',')], coords))

    # expanded to detect areas spanning to infinity
    min_x = min(coords, key=lambda x: x[0])[0] - 1
    min_y = min(coords, key=lambda x: x[1])[1] - 1

    max_x = max(coords, key=lambda x: x[0])[0] + 1
    max_y = max(coords, key=lambda x: x[1])[1] + 1

    grid = {}
    for x,y in product(range(min_x, max_x+1), range(min_y, max_y+1)):
        grid[(x,y)] = closest(x, y, coords)

    # filter coords that reach edges (they will expand to infinity)
    remove = set()
    for x in range(min_x, max_x+1):
        remove.add(grid[(x,min_y)])
        remove.add(grid[(x,max_y)])

    for y in range(min_y, max_y+1):
        remove.add(grid[(min_x,y)])
        remove.add(grid[(max_x,y)])

    # remove filtered out coords
    grid_filtered = {k: v for k, v in grid.items() if v not in remove}

    # count occurence
    counts = Counter(grid_filtered.values())
    r = max(counts.items(), key=lambda x: x[1])
    print(r)

def closest_limit(x, y, coords, limit=10000):
    d = 0

    for i, c in enumerate(coords):
        d += abs(c[0] - x) + abs(c[1] - y)
        if d >= limit:
            return 0

    return 1

def solve_day6b():

    coords = list(open("day6_input.txt"))
    coords = list(map(lambda x: x.rstrip('\n'), coords))
    coords = list(map(lambda x: [int(y) for y in x.split(',')], coords))

    expand = 0

    # expanded to detect areas spanning to infinity
    min_x = min(coords, key=lambda x: x[0])[0] - expand
    min_y = min(coords, key=lambda x: x[1])[1] - expand

    max_x = max(coords, key=lambda x: x[0])[0] + expand
    max_y = max(coords, key=lambda x: x[1])[1] + expand

    grid = {}
    for x,y in product(range(min_x, max_x+1), range(min_y, max_y+1)):
        r = closest_limit(x, y, coords)
        if r != 0:
            grid[(x,y)] = r

    # count occurence
    counts = Counter(grid.values())
    print(list(counts.values())[0])
    

if __name__ == '__main__':
    solve_day6a()
    solve_day6b()