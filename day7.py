import re

from itertools import product, chain
from collections import Counter

def solve_day7a():
    instructions = list(open("day7_input.txt"))
    instructions = list(map(lambda x: [x[36], x[5]], instructions))
    #print(instructions)

    step_ids = list(set(chain.from_iterable(instructions)))

    # Step [[Cond], done]
    steps = {}
    for s in step_ids:

        # conditions
        relevant = list(filter(lambda x: x[0] == s, instructions))
        #print(relevant)

        conditions = list(set(chain.from_iterable(map(lambda x: x[1], relevant))))
        #print(conditions)

        steps[s] = conditions

    #print(steps.items())

    # {'A': [False, ['C']], 'C': [False, []], 'D': [False, ['A']], 'B': [False, ['A']], 'E': [False, ['F', 'B', 'D']], 'F': [False, ['C']]}
    def get_next_step(steps):
        # find step
        candidates = sorted(list(filter(lambda x: len(x[1]) == 0, steps.items())))
        #print(candidates)

        chosen = candidates[0][0]

        # mark as done
        del steps[chosen]
        steps = {k: list(filter(lambda x: x != chosen, v)) for k, v in steps.items()}

        return chosen, steps

    order = ''
    while len(steps) > 0:
        chosen, steps = get_next_step(steps)
        order += chosen


    print(order)
    
def solve_day7b():
    instructions = list(open("day7_input.txt"))
    instructions = list(map(lambda x: [x[36], x[5]], instructions))

    step_ids = list(set(chain.from_iterable(instructions)))

    # Step [[Cond], done]
    steps = {}
    for s in step_ids:

        # conditions
        relevant = list(filter(lambda x: x[0] == s, instructions))
        conditions = list(set(chain.from_iterable(map(lambda x: x[1], relevant))))

        steps[s] = conditions

    total_time = 0
    num_workers = 5
    workers = [[0, None] for _ in range(num_workers)]

    while len(steps) > 0:
        candidates = sorted(list(filter(lambda x: len(x[1]) == 0, steps.items())))

        # remove jobs already in progress from candidates
        candidates = list(filter(lambda x: x[0] not in [w[1] for w in workers], candidates))

        # process workers
        for i,worker in enumerate(workers):
            workers[i][0] -= 1
            if workers[i][0] <= 0:
                # old job finished?
                if workers[i][1]:
                    #print('Finished ' + workers[i][1])

                    # remove from available steps
                    del steps[workers[i][1]]
                    steps = {k: list(filter(lambda x: x != workers[i][1], v)) for k, v in steps.items()}

                    workers[i][0] = 0
                    workers[i][1] = None


        for c in candidates:
            # find available worker
            for i,worker in enumerate(workers):
                _, work_item = worker

                # available worker
                if work_item == None:
                    # start new job
                    chosen = c[0]
                    #print('{0} starting {1}'.format(i, chosen))
                    workers[i][0] = 60 + ord(chosen) - ord('A')
                    workers[i][1] = chosen
                    break

        total_time += 1
        #print(steps)


    print(total_time)


if __name__ == '__main__':
    solve_day7a()
    solve_day7b()
