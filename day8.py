def solve_day8a():

    data_file = open("day8_input.txt")
    data = [int(x) for x in data_file.read().split(' ')]
    data_file.close()

    def parse(data):
        num_childs, num_metas = data[:2]
        data = data[2:]

        total_sum = 0

        # parse children
        for _ in range(num_childs):
            data, meta_sum = parse(data)
            total_sum += meta_sum
        
        # add this nodes meta sum
        total_sum += sum(data[:num_metas])

        # add childrens meta sum
        if num_childs == 0:
            return data[num_metas:], sum(data[:num_metas])
        
        return data[num_metas:], total_sum


    data, total_sum = parse(data)
    print(total_sum)

def solve_day8b():

    data_file = open("day8_input.txt")
    data = [int(x) for x in data_file.read().split(' ')]
    data_file.close()

    def parse(data):
        num_childs, num_metas = data[:2]
        data = data[2:]

        node_metas = []

        for _ in range(num_childs):
            data, meta_sum = parse(data)
            node_metas.append(meta_sum)
        
        if num_childs == 0:
            return data[num_metas:], sum(data[:num_metas])
        
        return data[num_metas:], sum(node_metas[m-1] for m in data[:num_metas] if m <= len(node_metas))


    data, sum_meta = parse(data)
    print(sum_meta)

if __name__ == '__main__':
    solve_day8a()
    solve_day8b()
